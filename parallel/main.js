const parallel = require('async/parallel');

const people = ['张三', '李四', '王五', '赵六'];

const funcArray = people.map((name, index) => {
    return (callback) => {
        console.log(`${name}完成任务需要${index + 1}秒`);

        setTimeout(
            function () {
                callback(null, name);
            },
            (index + 1) * 1000
        );
    };
});

parallel(
    funcArray,
    function (err, people) {
        if (err) {
            console.log('有人没有完成任务');

            return;
        }

        console.log(`${people.join(',')}此时都完成了任务！`);
    }
);
