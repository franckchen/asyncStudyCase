const forever = require('async/forever');

let count = 0;
const countNum = (next) => {
    setTimeout(
        () => {
            console.log(++count);

            next();
        },
        1000
    );
};

forever(
    countNum(),
    1000
);
