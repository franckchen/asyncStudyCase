const whilst = require('async/whilst');

let count = 0;
const countNum = (callback) => {
    count++;

    setTimeout(
        () => {
            console.log(count);
            callback(null, count);
        },
        1000
    );
};

whilst(
    () => {
        return count < 10;
    },
    countNum,
    function () {
        console.log('计数完成');
    }
);
