const waterfall = require('async/waterfall');

const count = (count = 0, callback) => {
    if (typeof count === 'function') {
        callback = count;

        count = 0;
    }

    setTimeout(
        () => {
            console.log(++count);
            callback(null, count);
        },
        1000
    );
};

waterfall([
    count,
    count,
    count,
    count,
    count,
    count
]);
