const series = require('async/series');

let count = 0;
const countNum = (callback) => {
    console.log(++count);

    setTimeout(
        function () {
            callback(null);
        },
        1000
    );
};

series(
    [
        countNum, countNum, countNum,
        countNum, countNum, countNum,
        countNum, countNum, countNum,
        countNum
    ],
    function (err) {
        if (err) {
            console.log('error');

            return;
        }

        console.log('计数完成');
    }
);
